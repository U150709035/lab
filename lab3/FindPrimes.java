public class FindPrimes {
    public static void main (String[] args) {
        int n = Integer.parseInt(args[0]);
        String  primeNumbers = "";
        for (int i = 1; i <= n; i++) {
            int counter=0;
            for(int num = i; num >= 1; num--) {
                if(i % num == 0) {
                    counter = counter + 1;
                }
            }
            if (counter == 2) {
                primeNumbers = primeNumbers + i + ",";
            }
        }
        System.out.println(primeNumbers.substring(0, primeNumbers.length() - 1));
    }
}