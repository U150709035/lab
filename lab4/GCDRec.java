public class GCDRec{

 public static void main(String[] args) {
     int a = Integer.parseInt(args[0]);
     int b = Integer.parseInt(args[1]);
     int result = GCDR(a,b);
     System.out.println(result);
           } 


 public static int GCDR(int a, int b) {

     if (b == 0 ) {   
         return a+b; 
         }
         
     return GCDR(b, a % b); 
     } 

}