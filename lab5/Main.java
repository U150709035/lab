public class Main {

	public static void main(String[] args) {
		Rectangle rec = new Rectangle(5, 6);

		System.out.println("Side A: " + rec.sideA + " Side B: " + rec.sideB);
		System.out.println("Area: " + rec.area());
		System.out.println("Perimeter: " + rec.perimeter());

		Circle circle = new Circle(10);
		System.out.println();
		System.out.println("Radius: " + circle.radius);
		System.out.println("Area: " + circle.area());
		System.out.println("Perimeter: " + circle.perimeter());

	}

}